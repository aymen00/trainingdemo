import React , {Component} from 'react';
import logo from '../logo.jpg';
import WelcomePage from './WelcomePage';
import EntriesPage from './EntriesPage';


export class Subscribtion extends Component{

constructor() {
    super();
    this.state={  isReady:  false};
    this.handleClick=this.handleClick.bind(this);
  }

   handleClick(e) {
    e.preventDefault();
    console.log('Le lien a été cliqué.');
    this.setState(state=>({
        isReady: !state.isReady
    }))
    }

  render() {


    const renderTextArea = ()=> { if (this.state.isReady) {
      return <EntriesPage/>;
    }else{
      return ;
    }
    }
  return (
    <div className="App">
    <header className="App-header">
    <img src={logo} className="App-logo" alt="logo" />
  
    
    <WelcomePage name="Jhon"/>
  </header>
      <button onClick={this.handleClick} >{this.state.isReady ? 'OFF' : 'ON'}</button>
     <h1>{renderTextArea()}</h1>
      
      
    </div>
  );
}
}