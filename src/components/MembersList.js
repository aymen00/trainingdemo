import React, {Component} from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import '../DisplayMembers.css';
class MembersList extends Component{

    constructor(props) {

        super(props);

        this.state = {
            items: [],
            isLoaded: false
        }

    }

    async componentDidMount() {

        const url ='https://pixabay.com/api/?key=12413278-79b713c7e196c7a3defb5330e';
        fetch(url)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    items: json.hits,
                    isLoaded: true, 
                })
            }).catch((err) => {
                console.log(err);
            });

    }

    render() {

        var { items, isLoaded } = this.state;

        if (!isLoaded)
            return <div>Loading...</div>;

        return (
            <div className="container">
                <ListGroup>
                    {items.map(item => (
                        <ListGroupItem key={item.id}>
                            Id: {item.id} | Name: {item.user}
                        </ListGroupItem>
                    ))}
                </ListGroup>
            </div>
        );

    }

}
export default MembersList;