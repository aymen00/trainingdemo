import React , {Component} from 'react';

class WelcomePage extends Component {

    render() {

        return(
        <h1>Welcome Mr. {this.props.name}</h1>
        );
    }

}
export default WelcomePage;