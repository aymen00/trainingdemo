import React , {Component} from 'react';
import  {observer} from 'mobx-react';
import  UserStore from './UserStore';
import LoginForm from './LoginForm';
import SubmitButton from './SubmitButton';
import './EntriesPage.css'

class EntriesPage extends Component{

    async componentDidMount(){
        try {
            let res = await fetch('/isLoggedIn', {
                method: 'post',
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });

            let result = await res.json();
            if (result  &&  result.success){
                UserStore.loading =false;
                UserStore.isLoggedIn =true;
                UserStore.userName =result.userName;
            }
            else{
                UserStore.loading =false;
                UserStore.isLoggedIn=false;
            }

        }
        catch(e){
            UserStore.loading =false;
            UserStore.isLoggedIn =false;
        }
    }



    async doLogOut(){
        try {
            let res = await fetch('/logout', {
                method: 'post',
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });

            let result = await res.json();
            if (result  &&  result.success){
                UserStore.isLoggedIn =false;
                UserStore.userName='';
            }
            

        }
        catch(e){
           console.log(e);
        }
    }

    render(){

        if(UserStore.loading){
        return (
            <div className="entriePage">
                <div className="container">
                    Loading ! Please wait ..
                </div>
            </div>
        
        );
        }
        else{
            if(UserStore.isLoggedIn){
                return (
                    <div className="entriePage">
                        <div className="container">
                           welcome {UserStore.userName}
                           <SubmitButton    
                           text={'Logout'}  
                           disabled={false} 
                           onClick={ () =>  this.doLogOut()}    />
                        </div>
                    </div>
                
                );
            }
            return (
                <div className="entriePage">
                    <div className="container">
                        
                        <LoginForm  />
                    </div>
                </div>
            );
        }
    }
    

}
export default observer(EntriesPage);