import React, { Component } from 'react';
import './App.css';
import { Subscribtion } from './components/Subscribtion';
import { NavigationBar } from './components/NavigationBar';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import WelcomePage from './components/WelcomePage';
import MembersList from './components/MembersList'
import SearchMember from './components/SearchMember';

function App() {

    return(
        <BrowserRouter>
          <div className="container">
            <h5 className="m-3 d-flex justify-content-center">Members Management Portal</h5>

            <NavigationBar  />

            <Switch>
              <Route path='/' component={WelcomePage} exact />
              <Route path='/Intro' component={WelcomePage} />
              <Route path='/Subscribe' component={Subscribtion} />
              <Route path='/MembersList' component={MembersList} />
              <Route path='/SearchMember' component={SearchMember} />
            </Switch>
          </div>
        </BrowserRouter>

    );

}

export default App;
